#include <stdio.h>

int main()
{
    FILE* output = fopen("assignment9.txt", "w");
    FILE* input = fopen("assignment9.txt", "r");
    char buffer[200];

    // writing into the file
    fprintf(output, "UCSC is one of the leading institutes in Sri Lanka for computing studies\n");
    fclose(output);
    
    // reading the file
    fscanf(input, "%[^\n]%*c", buffer);
    puts(buffer);
    fclose(input);

    // appending text to the file
    output = fopen("assignment9.txt", "a");
    fprintf(output, "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.\n");
    fclose(output);

    return 0;
}

